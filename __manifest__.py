# Copyright 2019 Vauxoo
# License AGPL-3 or later (http://www.gnu.org/licenses/agpl).
{
    'name': "directory",
    'summary': """
        Odoo module to telephone directory Cotesma webpage""",
    'author': "Vauxoo",
    'website': "http://www.cotesma.coop",
    'category': 'Installer',
    'license': 'AGPL-3',
    'version': '12.0.1.0.0',
    'depends': ['website'],
    'data': [
        'security/ir.model.access.csv',
        'templates/directory_telephone_template.xml',
        'views/directory_telephone_views.xml',
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
