from odoo.tests.common import TransactionCase


class GlobalTestTelephone(TransactionCase):

    def setUp(self):
        super(GlobalTestTelephone, self).setUp()
        self.directory = self.env['directory.telephone']

    def test_telephone_create(self):
        """Test Create a directory telephone."""

        directory_id = self.directory.create({
            'name': 'test',
            'phone': '0001000000',
            'street': 'test street test 00'
        })

        self.assertTrue(directory_id)
        return True

    def test_telephone_search_full_text(self):
        """Test to search fulltext by all fields."""

        records = self.directory._search_full_text('carlos garcia')

        self.assertTrue(records)
        return True

    def test_telephone_search_full_text_whith_error(self):
        """Test to search fulltext by all fields with paramaters
        not type string."""

        records = self.directory._search_full_text(2)

        self.assertFalse(records)
        return False
