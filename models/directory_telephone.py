# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help

import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)

LIMIT = 10


class DirectoryTelephone(models.Model):
    _name = 'directory.telephone'
    _description = 'Directory Telephone'

    name = fields.Char(index=True, required=True)
    phone = fields.Char(required=True)
    street = fields.Text()
    active = fields.Boolean(help="Activate or deactivate record", default=True)

    def _search_full_text(self, text=''):
        if not isinstance(text, str):
            return ''

        received = text
        list_fields = ['name', 'street', 'phone']
        record_set = self
        parameters = received.split() if len(received) > 1 else received
        limit = LIMIT if not received else 0

        for field in list_fields:
            domain = []
            domain.append(('active', '=', True))
            if len(parameters) > 1:
                domain.append('|')

            for parameter in parameters:
                string_search = '("{0}", "ilike", "{1}")'.format(field,
                                                                 parameter)
                domain.append(eval(string_search))

            search = self.search(domain, limit=limit)
            record_set = record_set | search

        return record_set or ''
