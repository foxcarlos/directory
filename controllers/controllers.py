
from odoo import http


class TelephoneDirectory(http.Controller):

    @http.route('/page/guia', auth='public', website=True)
    def directory_telephone_list(self):
        telephone_records = ''

        telephone_records = http.request.env[
            'directory.telephone'].sudo()._search_full_text()

        return http.request.render('directory.telephone_page_template',
                                   {'telephones': telephone_records})

    @http.route('/page/telephone_search/', type='http', methods=['POST'],
                auth='public', website=True, csrf=True)
    def directory_telephone_post(self, **kw):
        received = kw.get('name')
        telephone_records = http.request.env[
            'directory.telephone'].sudo()._search_full_text(received)

        return http.request.render('directory.telephone_page_template',
                                   {'telephones': telephone_records})
