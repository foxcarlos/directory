==============
{telephone_directory}
==============

Odoo Module for show Client telephone directory in Cotesma web Page.

Configuration
=============

To configure this module, you need to:

Library Python installed:

MS SQL


Usage
=====

To use this module, you need to:

* Verify Engine database exists: Menu -> Engine database -> Engine DB List



Known issues / Roadmap
======================

* 


Credits
=======

Contributors
------------

* Carlos Garcia <foxcarlos@gmail.com>

Maintainer
----------

This module is maintained by the COTESMA.

